## Introduce

Fountain is an static blog generator developed by Golang.

## Usage

```
./fountain -r example/ -v -c
```

## 深色主题

![Dark theme](./example/themes/default/static/screen.png)

## 浅色主题

![Light theme](./example/themes/larecipe/static/screen.png)
